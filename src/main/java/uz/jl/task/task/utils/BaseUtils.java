package uz.jl.task.task.utils;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;




@Component
@RequiredArgsConstructor
public class BaseUtils {

//    private final TextEncryptor encryptor;


    public String generateActivationToken(@NonNull Long id) {
        return generateActivationToken(String.valueOf(id));
    }

    public String generateActivationToken(@NonNull String text) {
        return UUID.randomUUID().toString();
    }

    public String decode(@NonNull String text) {
        return "";

    }


}

