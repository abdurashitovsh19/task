package uz.jl.task.task.domains.driver;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import uz.jl.task.task.domains.Auditable;
import uz.jl.task.task.domains.vehicle.Vehicle;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;


@Entity
@Table(name = "vehicle_driver")
@Getter
@Setter
@RequiredArgsConstructor
public class VehicleDriver extends Auditable {

    /**
     * The full name of the driver.
     */
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "driver_id", updatable = false, nullable = false)
    @NotNull
    @Size(min = 2, max = 50)
    private String driverId;

    /**
     * The driver's unique license number.
     */
    @NotNull
//    @Size(min = 5, max = 20)
    @Column(nullable = false, unique = true)
    private String licenseNumber;

//    /**
//     * The vehicle currently assigned to the driver, if any.
//     */
//    @OneToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "vehicle_id")  // Corrected from "truck_id" to match the class name
//    private Vehicle vehicle;

    /**
     * The expiration date of the driver's license.
     */
    @NotNull
    @Column(nullable = false)
    private LocalDate licenseExpiryDate;

    /**
     * Indicates whether the driver is currently on duty.
     */
    @Column(columnDefinition = "boolean default false")
    private boolean onDuty;

    /**
     * The driver's total years of driving experience.
     */
    @NotNull
    @Min(0)
    private int experience;

    /**
     * The driver's performance rating (0.0 to 5.0).
     */
    @DecimalMin("0.0")
    @DecimalMax("5.0")
    private double rating;

    /**
     * The date the driver was employed.
     */
    @NotNull
    private LocalDate employmentDate;


}
