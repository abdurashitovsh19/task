package uz.jl.task.task.domains.vehicle;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import uz.jl.task.task.domains.Auditable;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
@Table(name = "vehicle")
@Getter
@Setter
@RequiredArgsConstructor
public class Vehicle extends Auditable {


    /**
     * The unique identifier for the vehicle.
     */
    @NotBlank
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "registrationNumber", updatable = false, nullable = false)
    @NotNull
    private String registrationNumber;

    /**
     * The model or make of the vehicle.
     */
    @NotBlank
    @Size(min = 2, max = 50)
    private String model;

    /**
     * The manufacturing year of the vehicle.
     */
    @NotNull
    @Min(1900)
    private int manufacturingYear;

    /**
     * The maximum weight capacity of the vehicle in kilograms.
     */
    @NotNull
    @Min(0)
    private double maxWeightCapacity;

    /**
     * The current mileage of the vehicle in kilometers.
     */
    @NotNull
    @Min(0)
    private double currentMileage;

    /**
     * The date when the vehicle was purchased.
     */
    @NotNull
    private LocalDate purchaseDate;

    @NotNull
    private Integer odometer;
    @NotNull
    private Integer engineHour;


    /**
     * The seniority level of the driver assigned to this vehicle.
     */


    /**
     * Indicates whether the vehicle is currently in use.
     */
    @Column(columnDefinition = "boolean default false")
    private boolean inUse;


}
