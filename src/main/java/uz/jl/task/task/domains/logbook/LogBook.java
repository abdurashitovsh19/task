package uz.jl.task.task.domains.logbook;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import uz.jl.task.task.domains.Auditable;
import uz.jl.task.task.domains.driver.VehicleDriver;
import uz.jl.task.task.domains.location.Location;
import uz.jl.task.task.domains.vehicle.Vehicle;
import uz.jl.task.task.enums.LogBookType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@Entity
@Table(name = "LogBook")
@Setter
@Getter
@RequiredArgsConstructor
public class LogBook extends Auditable {

    /**
     * The time zone in which the logbook entry was created.
     */
    @NotNull
    @Column(nullable = false)
    private ZoneId timeZone;

    /**
     * The driver associated with the logbook entry.
     */
    @ManyToOne
    @JoinColumn(name = "driver_id")
    private VehicleDriver driver;

    /**
     * The timestamp when the logbook entry started.
     */

    private Instant startedAt;

    /**
     * The timestamp when the logbook entry ended (optional).
     */
    private Instant endedAt;
    /**
     * The vehicle associated with the logbook entry.
     */
    @ManyToOne
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;


    /**
     * The total distance traveled during the logbook entry (in kilometers).
     */
    @Column(nullable = true)
    private Double distanceTraveled;

    /**
     * The total fuel consumed during the logbook entry (in liters).
     */
    @Column(nullable = true)
    private Double fuelConsumed;

    /**
     * Any notes or comments associated with the logbook entry.
     */
    @Column(columnDefinition = "TEXT")
    private String notes;

    @OneToMany
    @JoinColumn(name = "log_book_id")
    private List<ChangeAbleLogBook> changeAbleLogBook;

}

