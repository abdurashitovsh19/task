package uz.jl.task.task.domains.location;

import lombok.*;
import uz.jl.task.task.domains.Auditable;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Table(name = "locations")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Location extends Auditable {


    /**
     * The unique identifier for the location.
     */
    @NotBlank
    @Size(min = 5, max = 20)
    @Column(nullable = false, unique = true)
    private String locationCode;

    /**
     * The name or description of the location.
     */
    @NotBlank
    @Size(min = 2, max = 50)
    private String name;

    /**
     * The latitude of the location.
     */
    @NotNull
    private Double latitude;

    /**
     * The longitude of the location.
     */
    @NotNull
    private Double longitude;

    /**
     * The date when the location was established or opened.
     */
    @NotNull
    private LocalDate establishmentDate;


}
