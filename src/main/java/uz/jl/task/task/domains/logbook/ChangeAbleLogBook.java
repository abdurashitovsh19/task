package uz.jl.task.task.domains.logbook;

import lombok.*;
import lombok.experimental.SuperBuilder;
import uz.jl.task.task.domains.Auditable;
import uz.jl.task.task.domains.location.Location;
import uz.jl.task.task.enums.LogBookType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Represents a changeable logbook entry capturing modifications to a logbook event.
 */
@Entity
@Table(name = "ChangeAbleLogBook")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ChangeAbleLogBook extends Auditable {

    /**
     * The type of logbook entry modification.
     */
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, updatable = false)
    private LogBookType type;
    /**
     * The time zone in which the logbook entry was created.
     */
    @NotNull
    @Column(nullable = false)
    private ZoneId timeZone;
    /**
     * The timestamp when the logbook entry modification started.
     */
//    private Instant startedAt;
    private LocalDateTime startedAt;
    /**
     * The timestamp when the logbook entry modification ended.
     */
//    private Instant endedAt;
    private LocalDateTime endedAt;

    /**
     * The location associated with the logbook entry modification.
     */
    @ManyToOne
    @JoinColumn(name = "location_id")
    private Location location;

}

