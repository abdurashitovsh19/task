package uz.jl.task.task.domains.auth;

import lombok.*;

import javax.persistence.*;



@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_detail")
public class UserDetails {

    @Id
    @Column(unique = true, nullable = false)
    private Long userId;



    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    private String patronymic;

    private String fullName;
}
