package uz.jl.task.task.loader;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.jl.task.task.domains.auth.AuthUser;
import uz.jl.task.task.domains.driver.VehicleDriver;
import uz.jl.task.task.domains.location.Location;
import uz.jl.task.task.domains.logbook.ChangeAbleLogBook;
import uz.jl.task.task.domains.logbook.LogBook;
import uz.jl.task.task.domains.vehicle.Vehicle;
import uz.jl.task.task.enums.LogBookType;
import uz.jl.task.task.repositories.AuthUserRepository;
import uz.jl.task.task.repositories.changeAbleLogBook.ChangeAbleLogBookRepository;
import uz.jl.task.task.repositories.location.LocationRepository;
import uz.jl.task.task.repositories.logBook.LogBookRepository;
import uz.jl.task.task.repositories.vehicle.VehicleRepository;
import uz.jl.task.task.repositories.vehicleDriver.VehicleDriverRepository;

import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {
    private final AuthUserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final VehicleRepository vehicleRepository;
    private final VehicleDriverRepository vehicleDriverRepository;
    private final LogBookRepository logBookRepository;

    private final ChangeAbleLogBookRepository changeAbleLogBookRepository;
    private final LocationRepository locationRepository;
    Random random = new Random();

    public AuthUser uploadUser() {
        AuthUser authUser = AuthUser.childBuilder()
                .password("123")
                .username("ali")
                .email("abdurashitovsh19@gmail.com")
                .build();


        authUser.setPassword(passwordEncoder.encode(authUser.getPassword()));
        authUser.setCreatedBy(-1L);
        authUser.setStatus(AuthUser.Status.ACTIVE);
        userRepository.save(authUser);
        return authUser;
    }

    public List<Vehicle> createRandomVehicles(int numVehicles, Long createdBy) {
        return IntStream.range(0, numVehicles)
                .mapToObj(i -> {
                    String registrationNumber = generateRandomRegistrationNumber(random);
                    String model = generateRandomModel(random);
                    int manufacturingYear = random.nextInt(LocalDate.now().getYear() - 1900) + 1900;
                    double maxWeightCapacity = random.nextDouble() * 4000 + 1000; // 1000kg to 5000kg
                    double currentMileage = random.nextDouble() * 200000; // 0 to 200,000 km
                    LocalDate purchaseDate = LocalDate.now().minusYears(random.nextInt(5) + 1); // 1 to 5 years ago
                    int odometer = random.nextInt(200000); // 0 to 200,000
                    int engineHour = random.nextInt(10000); // 0 to 10,000
                    boolean inUse = random.nextBoolean();

                    Vehicle vehicle = new Vehicle();
                    vehicle.setRegistrationNumber(UUID.randomUUID().toString());
                    vehicle.setModel(model);
                    vehicle.setCreatedBy(createdBy);
                    vehicle.setManufacturingYear(manufacturingYear);
                    vehicle.setMaxWeightCapacity(maxWeightCapacity);
                    vehicle.setCurrentMileage(currentMileage);
                    vehicle.setPurchaseDate(purchaseDate);
                    vehicle.setOdometer(odometer);
                    vehicle.setEngineHour(engineHour);
                    vehicle.setInUse(inUse);
                    vehicle.setCreatedAt(LocalDateTime.now()); // Set createdAt for auditing

                    return vehicle;
                })
                .collect(Collectors.toList());
    }

    private static String generateRandomRegistrationNumber(Random random) {
        // Implement logic to generate a random registration number based on your requirements
        return "ABC" + random.ints(4, 1000, 9999).findFirst().getAsInt();
    }

    private static String generateRandomModel(Random random) {
        // Implement logic to generate a random model name based on your preferences
        String[] models = {"Toyota Camry", "Honda Accord", "Ford F-150", "Chevy Silverado", "Tesla Model 3"};
        return models[random.nextInt(models.length)];
    }

    public List<VehicleDriver> createRandomVehicleDrivers(int numDrivers, List<Vehicle> availableVehicles, Long createdBy) {
        return IntStream.range(0, numDrivers)
                .mapToObj(i -> {
                    String driverId = generateRandomDriverId(random);
                    String licenseNumber = generateRandomLicenseNumber(random);
                    LocalDate licenseExpiryDate = LocalDate.now().plusYears(random.nextInt(3) + 1);
                    boolean onDuty = random.nextBoolean();
                    int experience = random.nextInt(10) + 1;
                    double rating = random.nextDouble() * 5;
                    LocalDate employmentDate = LocalDate.now().minusYears(random.nextInt(5) + 1);

                    VehicleDriver driver = new VehicleDriver();
                    driver.setDriverId(driverId);
                    driver.setLicenseNumber(UUID.randomUUID().toString());
                    driver.setLicenseExpiryDate(licenseExpiryDate);
                    driver.setOnDuty(onDuty);
                    driver.setExperience(experience);
                    driver.setRating(rating);
                    driver.setEmploymentDate(employmentDate);

                    driver.setCreatedBy(createdBy);

//                    // Assign a random vehicle to the driver, if available
//                    if (!availableVehicles.isEmpty()) {
//                        int randomVehicleIndex = random.nextInt(availableVehicles.size());
//                        driver.setVehicle(availableVehicles.get(randomVehicleIndex));
//                        availableVehicles.remove(randomVehicleIndex); // Remove the assigned vehicle from the pool
//                    }

                    return driver;
                })
                .collect(Collectors.toList());
    }

    private static String generateRandomDriverId(Random random) {
        // Implement logic to generate a random driver ID based on your requirements
        return "DRV" + random.ints(6, 100000, 999999).findFirst().getAsInt();
    }

    private static String generateRandomLicenseNumber(Random random) {
        // Implement logic to generate a random license number based on your preferences
        return "ABC" + random.ints(4, 1000, 9999).findFirst().getAsInt();
    }

    public ZoneId randomZoneId() {

//        List<ZoneId> avaibleZoneIdList = ZoneId.getAvailableZoneIds().stream().map(ZoneId::of).toList();

//        ZoneId zoneId = avaibleZoneIdList.get(random.nextInt(avaibleZoneIdList.size()));

        return ZoneId.of("America/Chicago");

    }

    public Location createRandomLocation(Long createdBy) {

        String locationCode = generateRandomCode(random);
        String name = generateRandomName(random);
        double latitude = generateRandomLatitude(random);
        double longitude = generateRandomLongitude(random);
        LocalDate establishmentDate = generateRandomEstablishmentDate(random);

        Location entity = new Location(locationCode, name, latitude, longitude, establishmentDate);
        entity.setCreatedBy(createdBy);
        return locationRepository.save(entity);
    }

    private static String generateRandomName(Random random) {
        // Generate a random name of length 2-50
        int nameLength = random.nextInt(49) + 2; // Ensure length is between 2 and 50
        StringBuilder nameBuilder = new StringBuilder();
        for (int i = 0; i < nameLength; i++) {
            char c = (char) (random.nextInt(26) + 97); // Generate lowercase letters
            nameBuilder.append(c);
        }
        return nameBuilder.toString(); // Capitalize first letter
    }

    private static double generateRandomLatitude(Random random) {
        // Generate a random latitude within valid range (-90 to 90)
        return random.nextDouble() * 180 - 90;
    }

    private static double generateRandomLongitude(Random random) {
        // Generate a random longitude within valid range (-180 to 180)
        return random.nextDouble() * 360 - 180;
    }

    private static LocalDate generateRandomEstablishmentDate(Random random) {
        // Generate a random date within a reasonable range (e.g., past 50 years)
        int years = 2020;
        int month = 1;
        int day = random.nextInt(1, 3);

        return LocalDate.of(years, month, day);
    }

    private static String generateRandomCode(Random random) {
        // Generate a random alphanumeric code of length 5-20
        int codeLength = random.nextInt(16) + 5; // Ensure length is between 5 and 20
        StringBuilder codeBuilder = new StringBuilder();
        for (int i = 0; i < codeLength; i++) {
            char c = (char) (random.nextInt(43) + 48); // Generate alphanumeric characters
            codeBuilder.append(c);
        }
        return codeBuilder.toString();
    }

    public List<LogBook> createRandomLogBooks(int numLogBooks, List<VehicleDriver> drivers, List<Vehicle> vehicles, Long createdBy) {

        return IntStream.range(0, numLogBooks)
                .mapToObj(i -> {
                    VehicleDriver driver = drivers.get(random.nextInt(drivers.size()));
                    Vehicle vehicle = vehicles.get(random.nextInt(vehicles.size()));
                    Instant startedAt = generateRandomEstablishmentDate(random).atStartOfDay().atZone(randomZoneId()).toInstant();
                    Instant endedAt = startedAt.plusSeconds(random.nextInt(60 * 60 * 8000)); // Up to 8 hours duration
                    Double distanceTraveled = random.nextDouble() * 500; // 0 to 500 km
                    Double fuelConsumed = random.nextDouble() * 50; // 0 to 50 liters
                    String notes = "Random logbook entry " + i;

                    LogBook logBook = new LogBook();
                    logBook.setTimeZone(randomZoneId());
                    logBook.setDriver(driver);
                    logBook.setVehicle(vehicle);
                    logBook.setStartedAt(startedAt);
                    logBook.setEndedAt(endedAt);
                    logBook.setDistanceTraveled(distanceTraveled);
                    logBook.setFuelConsumed(fuelConsumed);
                    logBook.setCreatedBy(createdBy);
                    List<ChangeAbleLogBook> randomChangeAbleLogBooks = createRandomChangeAbleLogBooks(random.nextInt(10, 80), createdBy);
                    changeAbleLogBookRepository.saveAll(randomChangeAbleLogBooks);

                    logBook.setChangeAbleLogBook(randomChangeAbleLogBooks);
                    logBook.setNotes(notes);
                    logBook.setCreatedAt(LocalDateTime.now()); // Set createdAt for auditing
                    return logBook;
                })
                .collect(Collectors.toList());
    }


    public List<ChangeAbleLogBook> createRandomChangeAbleLogBooks(int numLogBooks, Long createdBy) {
        return IntStream.range(0, numLogBooks)
                .mapToObj(i -> {

                    LogBookType type = LogBookType.values()[random.nextInt(LogBookType.values().length)]; // Random type


//                    Instant startedAt =
//
//                            generateRandomEstablishmentDate(random)
//                                    .atStartOfDay(randomZoneId())
//                                    .toInstant()
//                                    .plusSeconds(random.nextInt(60 * 60 * 24)); // Up to 24 hours after original start

                    LocalDateTime startedAt = generateRandomEstablishmentDate(random).atStartOfDay(randomZoneId()).plusSeconds(random.nextInt(60 * 60 * 24)).toLocalDateTime();
                    LocalDateTime endedAt = startedAt.plusSeconds(random.nextInt(60 * 60 * 8)); // Up to 8 hours duration
                    Location location = createRandomLocation(createdBy); // Optional: Add logic to create a random location if needed

                    ChangeAbleLogBook changeAbleLogBook = new ChangeAbleLogBook();
                    changeAbleLogBook.setType(type);
                    changeAbleLogBook.setCreatedBy(createdBy);
                    changeAbleLogBook.setTimeZone(randomZoneId());
                    changeAbleLogBook.setStartedAt(startedAt);
                    changeAbleLogBook.setEndedAt(endedAt);
                    changeAbleLogBook.setLocation(location);
                    changeAbleLogBook.setCreatedAt(LocalDateTime.now()); // Set createdAt for auditing

                    return changeAbleLogBook;
                })
                .collect(Collectors.toList());
    }

    @Override
    public void run(String... args) throws Exception {
//        AuthUser authUser = uploadUser();
//        Long createdBy = authUser.getCreatedBy();
//        List<Vehicle> randomVehicles = createRandomVehicles(random.nextInt(50, 70), createdBy);
//        vehicleRepository.saveAll(randomVehicles);
//        List<VehicleDriver> vehicleDrivers = createRandomVehicleDrivers(random.nextInt(50, 70), randomVehicles, createdBy);
//        vehicleDriverRepository.saveAll(vehicleDrivers);
//        List<LogBook> randomLogBooks = createRandomLogBooks(random.nextInt(50, 200), vehicleDrivers, randomVehicles, createdBy);
//        logBookRepository.saveAll(randomLogBooks);


        uploadManual();
    }

    private void uploadManual() {

        AuthUser authUser = uploadUser();
        Long createdBy = authUser.getId();


        for (int i = 0; i < 10; i++) {

            Vehicle vehicle = new Vehicle();
            vehicle.setRegistrationNumber(UUID.randomUUID().toString());
            vehicle.setModel(generateRandomModel(random));
            vehicle.setCreatedBy(createdBy);
            vehicle.setManufacturingYear(1990);
            vehicle.setMaxWeightCapacity(2000);
            vehicle.setCurrentMileage(80000);
            vehicle.setPurchaseDate(LocalDate.now());
            vehicle.setOdometer(5000);
            vehicle.setEngineHour(8000);
            vehicle.setInUse(true);

            vehicle.setCreatedAt(LocalDateTime.now()); // Set createdAt for auditing

            vehicleRepository.save(vehicle);


            String driverId = generateRandomDriverId(random);
            String licenseNumber = generateRandomLicenseNumber(random);
            LocalDate licenseExpiryDate = LocalDate.now().plusYears(random.nextInt(3) + 1);
            boolean onDuty = random.nextBoolean();
            int experience = random.nextInt(10) + 1;
            double rating = random.nextDouble() * 5;
            LocalDate employmentDate = LocalDate.now().minusYears(random.nextInt(5) + 1);

            VehicleDriver vehicleDriver = new VehicleDriver();
            vehicleDriver.setDriverId(driverId);
            vehicleDriver.setLicenseNumber(licenseNumber);
            vehicleDriver.setLicenseExpiryDate(licenseExpiryDate);
            vehicleDriver.setOnDuty(onDuty);
            vehicleDriver.setExperience(experience);
            vehicleDriver.setRating(rating);
            vehicleDriver.setEmploymentDate(employmentDate);

            vehicleDriver.setCreatedBy(createdBy);
            vehicleDriverRepository.save(vehicleDriver);

            LocalDateTime startedAt = LocalDateTime.of(2020, 01, 01, 00, 00).atZone(randomZoneId()).toLocalDateTime();
            List<ChangeAbleLogBook> changeAbleLogBooks = new ArrayList<>();
            changeAbleLogBooks = randomChanableLogBook(createdBy, startedAt, changeAbleLogBooks, random.nextInt(4, 8));

            LogBook logBook = new LogBook();
            logBook.setNotes("Fake");
            logBook.setTimeZone(randomZoneId());
            logBook.setChangeAbleLogBook(changeAbleLogBooks);
            logBook.setFuelConsumed(50000D);
            logBook.setDriver(vehicleDriver);
            logBook.setStartedAt(Instant.now());
            logBook.setEndedAt(Instant.now().plusSeconds(80000));
            logBook.setVehicle(vehicle);

            logBook.setCreatedBy(createdBy);
            logBookRepository.save(logBook);

        }
    }

    private List<ChangeAbleLogBook> randomChanableLogBook(Long createdBy, LocalDateTime startedAt, List<ChangeAbleLogBook> all, int amount) {
        LogBookType type = LogBookType.values()[random.nextInt(LogBookType.values().length)]; // Random type

        LocalDateTime endedAt = startedAt.plusMinutes(random.nextInt(1, 1999));

        Location location = createRandomLocation(createdBy); // Optional: Add logic to create a random location if needed

        ChangeAbleLogBook changeAbleLogBook = new ChangeAbleLogBook();
        changeAbleLogBook.setType(type);
        changeAbleLogBook.setCreatedBy(createdBy);
        changeAbleLogBook.setTimeZone(randomZoneId());
        changeAbleLogBook.setStartedAt(startedAt);
        changeAbleLogBook.setEndedAt(endedAt);
        changeAbleLogBook.setLocation(location);
        changeAbleLogBook.setCreatedAt(LocalDateTime.now()); // Set createdAt for auditing

        changeAbleLogBookRepository.save(changeAbleLogBook);
        all.add(changeAbleLogBook);
        if (amount == 0) {

            return all;
        }

        return randomChanableLogBook(createdBy, endedAt, all, --amount);
    }

}
