package uz.jl.task.task.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;

public class Test {
    public static int[] mergeSameElements(int[] arr) {
        if (arr == null || arr.length <= 1) {
            return arr; // Handle empty or single-element arrays
        }

        int writeIndex = 0;
        int currentElement = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == currentElement) {
                // Skip merging if already merged with the previous element


                continue;
            }

            arr[writeIndex++] = currentElement;
            currentElement = arr[i];
        }

        // Copy the last element if it's not already in the merged array
        if (writeIndex < arr.length - 1 || arr[writeIndex] != currentElement) {
            arr[writeIndex++] = currentElement;
        }

        // Create a new array with the appropriate size to avoid extra space
        int[] mergedArr = new int[writeIndex];
        System.arraycopy(arr, 0, mergedArr, 0, writeIndex);
        return mergedArr;
    }

    public static void main(String[] args) {
        int[] arr = {1, 1, 2, 2, 3, 3, 3, 4};
        int[] mergedArr = mergeSameElements(arr);
        System.out.println(Arrays.toString(mergedArr)); // Output: [1, 2, 3, 4]

    }
}
