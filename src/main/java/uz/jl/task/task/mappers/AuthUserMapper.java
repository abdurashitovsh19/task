package uz.jl.task.task.mappers;

import org.mapstruct.Mapper;
import uz.jl.task.task.domains.auth.AuthUser;
import uz.jl.task.task.dtos.user.AuthUserCreateDTO;
import uz.jl.task.task.dtos.user.AuthUserDTO;
import uz.jl.task.task.dtos.user.AuthUserUpdateDTO;



@Mapper(componentModel = "spring")
public interface AuthUserMapper extends BaseMapper<AuthUser, AuthUserDTO, AuthUserCreateDTO, AuthUserUpdateDTO> {

}
