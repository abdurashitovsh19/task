package uz.jl.task.task.mappers.vehicleDriverMapper;

import org.mapstruct.Mapper;
import uz.jl.task.task.controllers.vehicleDriver.VehicleDriver;
import uz.jl.task.task.dtos.driver.VehicleDriverCreateDTO;
import uz.jl.task.task.dtos.driver.VehicleDriverDTO;
import uz.jl.task.task.dtos.driver.VehicleDriverUpdateDTO;
import uz.jl.task.task.mappers.BaseMapper;

@Mapper(componentModel = "spring")
public interface VehicleDriverMapper extends BaseMapper<VehicleDriver, VehicleDriverDTO, VehicleDriverCreateDTO, VehicleDriverUpdateDTO> {
}
