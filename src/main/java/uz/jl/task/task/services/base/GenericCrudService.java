package uz.jl.task.task.services.base;

import uz.jl.task.task.criteria.GenericCriteria;
import uz.jl.task.task.dtos.Dto;
import uz.jl.task.task.dtos.GenericDto;

import java.io.Serializable;


public interface GenericCrudService<ID extends Serializable,
        D extends GenericDto,
        CD extends Dto,
        UD extends GenericDto,
        C extends GenericCriteria> extends GenericService<ID, CD, UD>, GenericReadService<ID, C, D> {
}
