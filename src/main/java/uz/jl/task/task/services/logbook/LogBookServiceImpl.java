package uz.jl.task.task.services.logbook;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.jl.task.task.domains.driver.VehicleDriver;
import uz.jl.task.task.domains.location.Location;
import uz.jl.task.task.domains.logbook.ChangeAbleLogBook;
import uz.jl.task.task.domains.logbook.LogBook;
import uz.jl.task.task.domains.vehicle.Vehicle;
import uz.jl.task.task.dtos.driver.VehicleDriverDTO;
import uz.jl.task.task.dtos.location.LocationDTO;
import uz.jl.task.task.dtos.logbook.LogBookCreateDTO;
import uz.jl.task.task.dtos.logbook.LogBookCriteria;
import uz.jl.task.task.dtos.logbook.LogBookDTO;
import uz.jl.task.task.dtos.logbook.LogBookUpdateDTO;
import uz.jl.task.task.dtos.logbook.response.ChangeAbleLogBookDTO;
import uz.jl.task.task.dtos.vehicle.VehicleDTO;
import uz.jl.task.task.enums.LogBookType;
import uz.jl.task.task.repositories.logBook.LogBookRepository;

import java.time.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LogBookServiceImpl implements LogBookService {
    private final LogBookRepository logBookRepository;

    @Override
    public List<LogBookDTO> getAll(@NonNull LogBookCriteria criteria) {
        return null;
    }

    @Override
    public LogBookDTO get(@NonNull Long aLong) {
        return null;
    }

    @Override
    public Long create(@NonNull LogBookCreateDTO dto) {
        return null;
    }

    @Override
    public void delete(@NonNull Long aLong) {

    }

    @Override
    public void update(@NonNull LogBookUpdateDTO dto) {

    }

    /**
     * Retrieves a list of LogBookDTOs, filtered and converted from LogBook entities based on driver ID, start date, and time zone.
     *
     * @param driverId   The ID of the driver for which to retrieve log books.
     * @param from       The start date and time in the user's time zone.
     * @param userZoneId The user's time zone.
     * @return A list of LogBookDTOs representing the filtered and converted log books.
     */
    @Override
    public List<LogBookDTO> getDto(String driverId, LocalDateTime from, ZoneId userZoneId) {

        // Retrieve and filter log books efficiently, filtering ChangeAbleLogBooks within the stream
        List<LogBook> filteredLogBooks = filterLogBooksByStartDateAndTimeZone(logBookRepository.findByVehicleDriverId(driverId), from, userZoneId);


        // Map filtered log books to DTOs, calculating total durations and using pre-calculated durations for ChangeAbleLogBookDTOs
        List<LogBookDTO> collect = filteredLogBooks.stream()
                .map(logBook -> {
                    List<ChangeAbleLogBookDTO> actions = mapChangeAbleLogBooksToDTOs(logBook.getChangeAbleLogBook(), userZoneId);
                    Double totalDuration = actions.stream().mapToDouble(ChangeAbleLogBookDTO::getDurationSec).sum();

                    return mapLogBookDto(logBook, userZoneId, actions, totalDuration);
                })
                .collect(Collectors.toList());


        for (LogBookDTO logBookDTO : collect) {
            List<ChangeAbleLogBookDTO> last_actions = new ArrayList<>();

            List<ChangeAbleLogBookDTO> actions = logBookDTO.getActions();

            mergeSameElements(actions);
            for (int i = 0; i < actions.size() - 1; i++) {
                int j = i;
                j += 1;
                ChangeAbleLogBookDTO currentAction = actions.get(i);
                ChangeAbleLogBookDTO nextAction = actions.get(j);
                if (currentAction.getType().equals(nextAction.getType())) {

                    currentAction.setDurationSec(currentAction.getDurationSec() + nextAction.getDurationSec());
                    currentAction.setEndedAt(nextAction.getEndedAt());
                    last_actions.add(currentAction);

                } else {
                    last_actions.add(currentAction);
                }
            }
            logBookDTO.setActions(last_actions);
        }

        return collect;
    }



    // Helper methods for mapping Vehicle and VehicleDriver to DTOs
    private VehicleDTO mapVehicleToDTO(Vehicle vehicle) {
        return VehicleDTO.builder()
                .model(vehicle.getModel())
                .inUse(vehicle.isInUse())
                .currentMileage(vehicle.getCurrentMileage())
                .engineHour(vehicle.getEngineHour())
                .manufacturingYear(vehicle.getManufacturingYear())
                .maxWeightCapacity(vehicle.getMaxWeightCapacity())
                .purchaseDate(vehicle.getPurchaseDate())
                .registrationNumber(vehicle.getRegistrationNumber())


                .build();
    }

    private LogBookDTO mapLogBookDto(LogBook logBook, ZoneId userZoneId, List<ChangeAbleLogBookDTO> actions, Double totalDuration) {
        return LogBookDTO.builder()
                .startedAt(logBook.getStartedAt().atZone(userZoneId).toLocalDateTime()) // Convert timestamps to user's time zone
                .endedAt(logBook.getEndedAt().atZone(userZoneId).toLocalDateTime())
                .actions(actions)
                .odometer(logBook.getVehicle().getOdometer())
                .time(Instant.now().atZone(userZoneId).toLocalDateTime()) // Use Instant.now() for concise current time
                .driver(mapVehicleDriverToDTO(logBook.getDriver()))
                .vehicle(mapVehicleToDTO(logBook.getVehicle()))
                .durationSec(totalDuration)
                .build();
    }


    public LocalDateTime convertLocalDateTime(LocalDateTime localDateTime, ZoneId fromTimeZone, ZoneId toTimeZone) {
        try {
            // Apply the original time zone to create a ZonedDateTime
            ZonedDateTime zonedDateTime = localDateTime.atZone(fromTimeZone);

            // Convert to the target time zone
            zonedDateTime = zonedDateTime.withZoneSameInstant(toTimeZone);

            // Extract the LocalDateTime from the converted ZonedDateTime
            return zonedDateTime.toLocalDateTime();

        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid time zone format.", e);
        }
    }

    private VehicleDriverDTO mapVehicleDriverToDTO(VehicleDriver driver) {
        return VehicleDriverDTO.builder()


                .onDuty(driver.isOnDuty())
                .employmentDate(driver.getEmploymentDate())
                .licenseNumber(driver.getLicenseNumber())
                .licenseExpiryDate(driver.getLicenseExpiryDate())
                .driverId(driver.getDriverId())
                .rating(driver.getRating())

                .build();
    }

    // Helper method to map ChangeAbleLogBooks to DTOs (already optimized in a previous response)
    private List<ChangeAbleLogBookDTO> mapChangeAbleLogBooksToDTOs(List<ChangeAbleLogBook> changeAbleLogBooks, ZoneId userZoneId) {
        List<ChangeAbleLogBookDTO> actions = new ArrayList<>();
        for (ChangeAbleLogBook changeAbleLogBook : changeAbleLogBooks) {

//            ZoneId timeZone = changeAbleLogBook.getTimeZone();


            ZoneId logBookTimeZone = changeAbleLogBook.getTimeZone();
            LocalDateTime startedAtWithOwnZone = changeAbleLogBook.getStartedAt().atZone(logBookTimeZone).toLocalDateTime();
            LocalDateTime started = convertLocalDateTime(startedAtWithOwnZone, logBookTimeZone, userZoneId);
            LocalDateTime endedAtWithOwnZone = changeAbleLogBook.getEndedAt().atZone(logBookTimeZone).toLocalDateTime();
            LocalDateTime ended = convertLocalDateTime(endedAtWithOwnZone, logBookTimeZone, userZoneId);

//            Duration duration = Duration.between(changeAbleLogBook.getStartedAt(), changeAbleLogBook.getEndedAt());

            Duration duration = Duration.between(started, ended);

            Location location = changeAbleLogBook.getLocation();
            actions.add(

                    ChangeAbleLogBookDTO.builder()
                            .id(changeAbleLogBook.getId())
                            .type(changeAbleLogBook.getType())
                            .durationSec((double) duration.getSeconds())
                            .startedAt(started)
                            .endedAt(ended)
                            .location(
                                    new LocationDTO(location.getName(), location.getLatitude(), location.getLongitude())
                            )
                            .timeZone(userZoneId)
                            .build());
        }

        actions.sort(Comparator.comparing(ChangeAbleLogBookDTO::getStartedAt));
        return actions;

    }


    // Helper method to filter log books based on start date and time zone
    private List<LogBook> filterLogBooksByStartDateAndTimeZone(List<LogBook> logBooks, LocalDateTime from, ZoneId userZoneId) {
        return logBooks.stream()
                .filter(logBook -> logBook.getStartedAt().atZone(userZoneId)
                        .withZoneSameInstant(userZoneId)
                        .toLocalDateTime()
                        .isAfter(from))
                .map(logBook -> {
                    // Filter ChangeAbleLogBooks within the stream for better performance
                    logBook.setChangeAbleLogBook(logBook.getChangeAbleLogBook().stream()
                            .filter(changeAbleLogBook -> changeAbleLogBook.getStartedAt()
                                    .atZone(userZoneId)
                                    .withZoneSameInstant(userZoneId)
                                    .toLocalDateTime()
                                    .isAfter(from))
                            .collect(Collectors.toList()));
                    return logBook;
                })
                .collect(Collectors.toList());
    }


    public List<ChangeAbleLogBookDTO> mergeSameElements(List<ChangeAbleLogBookDTO> arr) {
        return arr;
    }

}
