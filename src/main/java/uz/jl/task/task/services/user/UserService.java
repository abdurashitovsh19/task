package uz.jl.task.task.services.user;

import uz.jl.task.task.criteria.AuthUserCriteria;
import uz.jl.task.task.dtos.user.AuthUserCreateDTO;
import uz.jl.task.task.dtos.user.AuthUserDTO;
import uz.jl.task.task.dtos.user.AuthUserUpdateDTO;
import uz.jl.task.task.services.base.GenericCrudService;


public interface UserService extends GenericCrudService<
        Long,
        AuthUserDTO,
        AuthUserCreateDTO,
        AuthUserUpdateDTO,
        AuthUserCriteria> {
}
