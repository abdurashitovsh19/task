package uz.jl.task.task.services.base;

import lombok.NonNull;
import uz.jl.task.task.dtos.Dto;
import uz.jl.task.task.dtos.GenericDto;

import java.io.Serializable;


public interface GenericService<ID extends Serializable, CD extends Dto, UD extends GenericDto> extends BaseService {

    ID create(@NonNull CD dto);

    void delete(@NonNull ID id);

    void update(@NonNull UD dto);

}
