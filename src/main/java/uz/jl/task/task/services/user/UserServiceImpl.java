package uz.jl.task.task.services.user;

import lombok.NonNull;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.jl.task.task.criteria.AuthUserCriteria;
import uz.jl.task.task.domains.auth.AuthUser;
import uz.jl.task.task.dtos.user.AuthUserCreateDTO;
import uz.jl.task.task.dtos.user.AuthUserDTO;
import uz.jl.task.task.dtos.user.AuthUserUpdateDTO;
import uz.jl.task.task.mappers.AuthUserMapper;
import uz.jl.task.task.repositories.AuthUserRepository;
import uz.jl.task.task.services.base.AbstractService;
import uz.jl.task.task.utils.BaseUtils;

import java.util.List;


@Service
public class UserServiceImpl extends AbstractService<AuthUserRepository, AuthUserMapper> implements UserService {

    private final PasswordEncoder passwordEncoder;
    private final ServerProperties serverProperties;

    public UserServiceImpl(AuthUserRepository repository,
                           AuthUserMapper mapper,
                           BaseUtils utils,
                           PasswordEncoder passwordEncoder, ServerProperties serverProperties) {
        super(repository, mapper, utils);
        this.passwordEncoder = passwordEncoder;
        this.serverProperties = serverProperties;
    }

    @Override
    public List<AuthUserDTO> getAll(@NonNull AuthUserCriteria criteria) {
        return null;
    }

    @Override
    public AuthUserDTO get(@NonNull Long id) {
        return null;
    }

    @Override
    public Long create(@NonNull AuthUserCreateDTO dto) {
        AuthUser authUser = mapper.fromCreateDto(dto);
        authUser.setPassword(passwordEncoder.encode(authUser.getPassword()));
        authUser.setCreatedBy(-1L);
        authUser.setStatus(AuthUser.Status.ACTIVE);
        repository.save(authUser);
        return authUser.getId();
    }


    @Override
    public void delete(@NonNull Long id) {

    }

    @Override
    public void update(@NonNull AuthUserUpdateDTO dto) {

    }
}
