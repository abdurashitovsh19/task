package uz.jl.task.task.services.base;

import lombok.RequiredArgsConstructor;
import uz.jl.task.task.mappers.BaseMapper;
import uz.jl.task.task.mappers.GenericMapper;
import uz.jl.task.task.repositories.GenericRepository;
import uz.jl.task.task.utils.BaseUtils;


@RequiredArgsConstructor
public abstract class AbstractService<R extends GenericRepository, M extends GenericMapper> {
    protected final R repository;
    protected final M mapper;
    protected final BaseUtils utils;
}
