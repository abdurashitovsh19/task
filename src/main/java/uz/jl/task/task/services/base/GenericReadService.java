package uz.jl.task.task.services.base;

import lombok.NonNull;
import uz.jl.task.task.criteria.GenericCriteria;
import uz.jl.task.task.dtos.GenericDto;

import java.io.Serializable;
import java.util.List;


public interface GenericReadService<ID extends Serializable, C extends GenericCriteria, D extends GenericDto> extends BaseService {
    List<D> getAll(@NonNull C criteria);

    D get(@NonNull ID id);
}
