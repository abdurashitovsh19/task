package uz.jl.task.task.services.driver;

import uz.jl.task.task.dtos.driver.VehicleDriverCreateDTO;
import uz.jl.task.task.dtos.driver.VehicleDriverCriteria;
import uz.jl.task.task.dtos.driver.VehicleDriverDTO;
import uz.jl.task.task.dtos.driver.VehicleDriverUpdateDTO;
import uz.jl.task.task.services.base.GenericCrudService;

public interface VehicleDriverService extends GenericCrudService<
        Long,
        VehicleDriverDTO,
        VehicleDriverCreateDTO,
        VehicleDriverUpdateDTO,
        VehicleDriverCriteria
        > {
}
