package uz.jl.task.task.services.logbook;

import uz.jl.task.task.dtos.driver.VehicleDriverCreateDTO;
import uz.jl.task.task.dtos.driver.VehicleDriverCriteria;
import uz.jl.task.task.dtos.driver.VehicleDriverDTO;
import uz.jl.task.task.dtos.driver.VehicleDriverUpdateDTO;
import uz.jl.task.task.dtos.logbook.LogBookCreateDTO;
import uz.jl.task.task.dtos.logbook.LogBookCriteria;
import uz.jl.task.task.dtos.logbook.LogBookDTO;
import uz.jl.task.task.dtos.logbook.LogBookUpdateDTO;
import uz.jl.task.task.services.base.GenericCrudService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.TimeZone;

public interface LogBookService extends GenericCrudService<
        Long,
        LogBookDTO,
        LogBookCreateDTO,
        LogBookUpdateDTO,
        LogBookCriteria
        > {


    List<LogBookDTO> getDto(String s, LocalDateTime date, ZoneId timeZone);
}

