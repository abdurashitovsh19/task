package uz.jl.task.task.services.jwt;


import uz.jl.task.task.config.security.SecurityUserDetails;


public interface TokenService {

    String generateToken(SecurityUserDetails userDetails);

    Boolean isValid(String token);


}
