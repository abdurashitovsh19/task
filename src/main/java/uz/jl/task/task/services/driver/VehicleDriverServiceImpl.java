package uz.jl.task.task.services.driver;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;
import uz.jl.task.task.domains.driver.VehicleDriver;
import uz.jl.task.task.dtos.driver.VehicleDriverCreateDTO;
import uz.jl.task.task.dtos.driver.VehicleDriverCriteria;
import uz.jl.task.task.dtos.driver.VehicleDriverDTO;
import uz.jl.task.task.dtos.driver.VehicleDriverUpdateDTO;
import uz.jl.task.task.mappers.vehicleDriverMapper.VehicleDriverMapper;
import uz.jl.task.task.repositories.vehicleDriver.VehicleDriverRepository;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VehicleDriverServiceImpl implements VehicleDriverService {
    private final VehicleDriverRepository vehicleDriverRepository;
    private final VehicleDriverMapper vehicleDriverMapper;

    @Override
    public List<VehicleDriverDTO> getAll(@NonNull VehicleDriverCriteria criteria) {
        List<VehicleDriver> allByOnDuty = vehicleDriverRepository.findAllByOnDuty(criteria.isOnDuty());
        List<VehicleDriverDTO> res = new ArrayList<>();
        for (VehicleDriver driver : allByOnDuty) {
            res.add(VehicleDriverDTO.builder()
                    .driverId(driver.getDriverId())
                    .employmentDate(driver.getEmploymentDate())
                    .licenseExpiryDate(driver.getLicenseExpiryDate())
                    .licenseNumber(driver.getLicenseNumber())
                    .onDuty(driver.isOnDuty())

                    .build());
        }

        return res;
    }

    @Override
    public VehicleDriverDTO get(@NonNull Long id) {
        return null;
    }

    @Override
    public Long create(@NonNull VehicleDriverCreateDTO dto) {
        return null;
    }

    @Override
    public void delete(@NonNull Long id) {

    }

    @Override
    public void update(@NonNull VehicleDriverUpdateDTO dto) {

    }
}
