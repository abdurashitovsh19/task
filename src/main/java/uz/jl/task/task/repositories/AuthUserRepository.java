package uz.jl.task.task.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.jl.task.task.domains.auth.AuthUser;

import java.util.Optional;


public interface AuthUserRepository extends JpaRepository<AuthUser, Long>, GenericRepository {
    Optional<AuthUser> findByUsername(String username);


}
