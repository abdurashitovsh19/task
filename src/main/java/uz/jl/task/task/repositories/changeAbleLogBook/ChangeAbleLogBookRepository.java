package uz.jl.task.task.repositories.changeAbleLogBook;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.jl.task.task.domains.logbook.ChangeAbleLogBook;

public interface ChangeAbleLogBookRepository extends JpaRepository<ChangeAbleLogBook, Long> {
}
