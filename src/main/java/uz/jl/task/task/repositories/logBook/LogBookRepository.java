package uz.jl.task.task.repositories.logBook;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.jl.task.task.domains.logbook.LogBook;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface LogBookRepository extends JpaRepository<LogBook, Long> {


    // Optimized query for finding logbooks by driver
    @Query("SELECT lb FROM LogBook lb JOIN lb.driver driver WHERE driver.driverId = :driverId")
    List<LogBook> findByVehicleDriverId(@Param("driverId") String driverId);


}
