package uz.jl.task.task.repositories.location;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.jl.task.task.domains.location.Location;

public interface LocationRepository extends JpaRepository<Location, Long> {
}
