package uz.jl.task.task.repositories.vehicleDriver;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.jl.task.task.domains.driver.VehicleDriver;

import java.util.List;

public interface VehicleDriverRepository extends JpaRepository<VehicleDriver, Long> {

    @Query("SELECT vd FROM VehicleDriver vd WHERE vd.onDuty = :duty")
    List<VehicleDriver> findAllByOnDuty(@Param("duty") boolean duty);
}

