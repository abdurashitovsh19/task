package uz.jl.task.task.repositories.vehicle;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.jl.task.task.domains.auth.AuthUser;
import uz.jl.task.task.domains.vehicle.Vehicle;
import uz.jl.task.task.repositories.GenericRepository;

public interface VehicleRepository extends JpaRepository<Vehicle, Long>, GenericRepository {
}
