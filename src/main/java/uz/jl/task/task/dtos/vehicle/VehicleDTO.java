package uz.jl.task.task.dtos.vehicle;

import lombok.*;

import javax.persistence.Column;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
public class VehicleDTO {

    private String registrationNumber;


    private String model;


    private int manufacturingYear;


    private double maxWeightCapacity;


    private double currentMileage;


    private LocalDate purchaseDate;


    private Integer engineHour;


    private boolean inUse;
}
