package uz.jl.task.task.dtos.auth.request;


public record RefreshTokenRequest(String refreshToken) {

}
