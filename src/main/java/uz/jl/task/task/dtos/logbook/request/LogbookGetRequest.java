package uz.jl.task.task.dtos.logbook.request;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;

import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.format.annotation.DateTimeFormat;

@Schema(description = "Request object for retrieving logbook data")
public record LogbookGetRequest(
        String driverId,
        LocalDateTime date,
        String timeZone) {
}