package uz.jl.task.task.dtos.user;

import uz.jl.task.task.domains.auth.AuthUser;
import uz.jl.task.task.dtos.GenericDto;


public class AuthUserDTO extends GenericDto {
    private String username;
    private AuthUser.Status status;
}
