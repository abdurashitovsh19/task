package uz.jl.task.task.dtos.logbook;

import lombok.*;
import uz.jl.task.task.domains.location.Location;
import uz.jl.task.task.domains.logbook.ChangeAbleLogBook;
import uz.jl.task.task.domains.vehicle.Vehicle;
import uz.jl.task.task.dtos.GenericDto;
import uz.jl.task.task.dtos.driver.VehicleDriverDTO;
import uz.jl.task.task.dtos.location.LocationDTO;
import uz.jl.task.task.dtos.logbook.response.ChangeAbleLogBookDTO;
import uz.jl.task.task.dtos.vehicle.VehicleDTO;
import uz.jl.task.task.enums.LogBookType;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LogBookDTO extends GenericDto {

    private LocalDateTime time;
    private List<ChangeAbleLogBookDTO> actions;


    private Integer odometer;

    private VehicleDriverDTO driver;

    private VehicleDTO vehicle;

    private LocalDateTime startedAt;
    private LocalDateTime endedAt;
    private Double durationSec;
}
