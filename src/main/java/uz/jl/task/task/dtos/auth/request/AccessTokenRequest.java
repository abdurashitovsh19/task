package uz.jl.task.task.dtos.auth.request;


public record AccessTokenRequest(
        String username,
        String password) {
}
