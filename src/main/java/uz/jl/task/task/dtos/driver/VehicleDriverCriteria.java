package uz.jl.task.task.dtos.driver;

import lombok.Getter;
import lombok.Setter;
import uz.jl.task.task.criteria.GenericCriteria;
import uz.jl.task.task.dtos.GenericDto;

@Getter
@Setter
public class VehicleDriverCriteria extends GenericCriteria {
    private boolean onDuty;

}
