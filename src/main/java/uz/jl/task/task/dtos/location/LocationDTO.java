package uz.jl.task.task.dtos.location;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LocationDTO {


    /**
     * The name or description of the location.
     */
    private String name;

    /**
     * The latitude of the location.
     */
    private Double latitude;

    /**
     * The longitude of the location.
     */
    private Double longitude;


}
