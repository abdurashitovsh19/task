package uz.jl.task.task.dtos.logbook.response;

import lombok.*;
import uz.jl.task.task.domains.location.Location;
import uz.jl.task.task.dtos.GenericDto;
import uz.jl.task.task.dtos.location.LocationDTO;
import uz.jl.task.task.enums.LogBookType;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ChangeAbleLogBookDTO extends GenericDto {


    /**
     * The type of logbook entry modification.
     */
    private LogBookType type;
    private Long id;
    /**
     * The time zone in which the logbook entry was created.
     */
    @NotNull
    @Column(nullable = false)
    private ZoneId timeZone;
    /**
     * The timestamp when the logbook entry modification started.
     */
    private LocalDateTime startedAt;

    /**
     * The timestamp when the logbook entry modification ended.
     */
    private LocalDateTime endedAt;

    /**
     * The location associated with the logbook entry modification.
     */
    @ManyToOne
    @JoinColumn(name = "location_id")
    private LocationDTO location;

    private Double durationSec;

    public static void main(String[] args) {
        Instant instant = Instant.now(); // Current UTC instant
        ZoneId zoneId = ZoneId.of("Europe/Paris"); // Desired timezone
        ZoneId tashkent = ZoneId.of("Asia/Tashkent"); // Desired timezone

        LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();
        LocalDateTime time = instant.atZone(tashkent).toLocalDateTime();
        System.out.println(localDateTime); // Output: 2024-01-17T04:55:30 (assuming current time in Paris)
        System.out.println(time); // Output: 2024-01-17T04:55:30 (assuming current time in Paris)

    }

}
