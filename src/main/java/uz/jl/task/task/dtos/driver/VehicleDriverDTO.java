package uz.jl.task.task.dtos.driver;

import lombok.*;
import uz.jl.task.task.dtos.GenericDto;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
public class VehicleDriverDTO extends GenericDto {
    private String driverId;
    private String licenseNumber;
    private LocalDate licenseExpiryDate;
    private boolean onDuty;
    private double rating;
    private LocalDate employmentDate;

}
