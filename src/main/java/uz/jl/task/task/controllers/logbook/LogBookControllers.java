package uz.jl.task.task.controllers.logbook;

import freemarker.template.utility.DateUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.jl.task.task.controllers.ApiController;
import uz.jl.task.task.dtos.logbook.LogBookDTO;
import uz.jl.task.task.dtos.logbook.request.LogbookGetRequest;
import uz.jl.task.task.services.logbook.LogBookService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@RestController

public class LogBookControllers extends ApiController<LogBookService> {


    public LogBookControllers(LogBookService service) {
        super(service);
    }

    @Operation(summary = "Retrieves logbook data", description = "Provides logbook information based on driver ID, date, and time zone")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Token generated successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid request data"),
            @ApiResponse(responseCode = "401", description = "Unauthorized")
    })
    @GetMapping("/getLogData")
    public ResponseEntity<List<LogBookDTO>> get(
            @RequestParam @Schema(description = "Time zone", example = "Europe/London") String zoneId,
            @RequestParam @Schema(description = "Driver ID", example = "driver123") String driverId,
            @RequestParam(value = "date", required = false) @Schema(description = "Date", example = "yyyy-MM-dd HH:mm:ss") String date) {


        // Parse date securely
        LocalDateTime dateTime = parseDateTime(date);

        // Retrieve logbook data
        List<LogBookDTO> logbookData = service.getDto(driverId, dateTime, ZoneId.of(zoneId));

        // Handle empty results and potential NullPointerExceptions
        return logbookData != null && !logbookData.isEmpty()
                ? new ResponseEntity<>(logbookData, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // Helper method for secure date parsing
    private LocalDateTime parseDateTime(String dateStr) {
        try {
            return dateStr == null || dateStr.isBlank()
                    ? null // Or set a default value
                    : LocalDateTime.parse(dateStr.trim(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        } catch (DateTimeParseException e) {
            // Handle invalid date format
            return LocalDate.parse(dateStr.trim(), DateTimeFormatter.ofPattern("yyyy-MM-dd")).atStartOfDay();
        }
    }
}

