package uz.jl.task.task.controllers.auth;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import uz.jl.task.task.controllers.ApiController;
import uz.jl.task.task.dtos.auth.request.AccessTokenRequest;
import uz.jl.task.task.dtos.auth.request.RefreshTokenRequest;
import uz.jl.task.task.dtos.auth.response.TokenResponse;
import uz.jl.task.task.services.auth.AuthService;


@RestController
@Tag(name = "Authentication", description = "Endpoints for managing authentication")
public class AuthController extends ApiController<AuthService> {

    public AuthController(AuthService service) {
        super(service);
    }

    @Operation(
            summary = "Generate access token",
            description = "Creates a new access token for a user."
    )
    @SecurityRequirement(name = "bearerAuth")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Token generated successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid request data"),
            @ApiResponse(responseCode = "401", description = "Unauthorized")
    })
    @PostMapping(value = API + V1 + "/auth/access/token")
    public ResponseEntity<TokenResponse> generateAccessToken(@RequestBody AccessTokenRequest request) {
        return new ResponseEntity<>(service.generateToken(request), HttpStatus.CREATED);
    }

    @Operation(
            summary = "Refresh access token",
            description = "Refreshes an existing access token."
    )
    @SecurityRequirement(name = "bearerAuth")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Token refreshed successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid request data"),
            @ApiResponse(responseCode = "401", description = "Unauthorized")
    })
    @PostMapping(value = API + V1 + "/auth/refresh/token")
        public ResponseEntity<TokenResponse> refreshAccessToken(@RequestBody RefreshTokenRequest request) {
        return new ResponseEntity<>(service.refreshToken(request), HttpStatus.CREATED);
    }
}
