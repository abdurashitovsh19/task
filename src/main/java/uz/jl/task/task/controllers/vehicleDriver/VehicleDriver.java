package uz.jl.task.task.controllers.vehicleDriver;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.jl.task.task.controllers.ApiController;
import uz.jl.task.task.dtos.driver.VehicleDriverCriteria;
import uz.jl.task.task.dtos.driver.VehicleDriverDTO;
import uz.jl.task.task.services.driver.VehicleDriverService;

import java.util.List;

@RestController

public class VehicleDriver extends ApiController<VehicleDriverService> {
    public VehicleDriver(VehicleDriverService service) {
        super(service);
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<VehicleDriverDTO>> getAll(VehicleDriverCriteria criteria) {

        return new ResponseEntity<>(service.getAll(criteria), HttpStatus.OK);
    }
}
